<?php

use App\Modules\Beer\Controllers\API\CrudController as BeerCrudController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/dashboard', function () {
    return view('dashboard');
})->middleware(['auth'])->name('dashboard');

require __DIR__ . '/auth.php';

Route::group(['prefix' => 'api'], function () {
    Route::group(['middleware' => 'isAdmin', 'prefix' => 'admin'], function () {
        Route::get('beers', [BeerCrudController::class, 'index']);
        Route::post('beers', [BeerCrudController::class, 'store']);
        Route::patch('beers/{id}', [BeerCrudController::class, 'update']);
        Route::delete('beers/{id}', [BeerCrudController::class, 'destroy']);

    });

    Route::get('beers', [BeerCrudController::class, 'index']);
    Route::get('beers/{id}', [BeerCrudController::class, 'show']);
});