<?php

namespace Tests\Feature;

use App\Models\User;
use App\Models\Beer;
use Tests\TestCase;
use Illuminate\Http\UploadedFile;
use Illuminate\Foundation\Testing\WithoutMiddleware;

class ApiTest extends TestCase
{
    use WithoutMiddleware;

    protected static $id;

    protected static $user;

    protected static $adminUser;

    protected static $wrongData;

    public function __construct(?string $name = null, array $data = [], $dataName = '')
    {
        static::$wrongData = [
            [
                'name' => [],
            ],
            [
                'name' => 'test',
                'description' => []
            ],
            [
                'name' => 'test',
                'image' => UploadedFile::fake()->image('image.png')
            ],
        ];

        static::$user = new User([
            'name' => 'test',
            'is_admin' => false,
        ]);

        static::$adminUser = new User([
            'name' => 'testAdmin',
            'email' => 'test_email@mail.ru',
        ]);

        static::$adminUser->is_admin = true;

        parent::__construct($name, $data, $dataName);
    }

    public function test_beer_create_and_show()
    {
        $this->withoutMiddleware('VerifyCsrfToken');
        $this->be(static::$adminUser);

        $data = [
            'name' => 'Bud' . rand(10, 100),
            'description' => 'cheap' . rand(10, 100),
            'image' => UploadedFile::fake()->image('image.jpg')
        ];

        $response = $this->postJson('/api/admin/beers', $data);

        $response->assertStatus(200)
            ->assertJson([
                'payload' => [
                    'name' => $data['name'],
                    'description' => $data['description']
                ],
            ]);

        $response->assertJsonStructure([
            'payload' => [
                'image'
            ]
        ]);

        $payload = json_decode($response->getContent())->payload;
        static::$id = $payload->id;

        $beer = Beer::find(static::$id);
        $this->assertTrue(is_file(base_path($beer->image)));
        // $this->get(url($payload->image))->assertStatus(200);

        $showResponse = $this->get('/api/beers/' . static::$id)
            ->assertExactJson(json_decode($response->getContent(), true));
    }

    public function test_negative_beer_create()
    {
        $this->withoutMiddleware('VerifyCsrfToken');
        $this->be(static::$adminUser);


        foreach (static::$wrongData as $data) {
            $this->postJson('/api/admin/beers', $data)->assertStatus(422);
        }
    }

    public function test_beer_update()
    {
        $this->withoutMiddleware('VerifyCsrfToken');
        $this->be(static::$adminUser);

        $data = [
            'name' => 'Клинское' . rand(10, 100),
            'description' => 'Еще дешевле' . rand(10, 100),
        ];

        $oldModel = Beer::find(static::$id);

        $response = $this->patchJson(
            '/api/admin/beers/' . static::$id,
            array_merge($data, ['image' => UploadedFile::fake()->image('image.jpg')])
        );

        $response->assertStatus(200)
            ->assertJson([
                'payload' => [
                    'name' => $data['name'],
                    'description' => $data['description'],
                ],
            ]);

        $updatedModel = Beer::find(static::$id);

        $this->assertTrue($updatedModel->name === $data['name']);
        $this->assertTrue($updatedModel->description === $data['description']);
        $this->assertTrue($updatedModel->image !== $oldModel->image);
    }

    public function test_negative_beer_update()
    {
        $this->withoutMiddleware('VerifyCsrfToken');
        $this->be(static::$adminUser);

        foreach (static::$wrongData as $data) {
            $this->patchJson('/api/admin/beers/' . static::$id, $data)->assertStatus(422);
        }
    }
}
