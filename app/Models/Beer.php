<?php


namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Beer extends Model
{
    protected $table = 'beers';

    protected $casts = [
        'name' => 'string',
        'description' => 'string',
        'image' => 'string',
    ];

    protected $fillable = [
        'name',
        'description',
        'image',
    ];
}