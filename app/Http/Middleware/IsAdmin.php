<?php

namespace App\Http\Middleware;

use Closure;

/**~ki
 * Проверка, есть ли доступ к странице инструмент менеджера.
 */
class IsAdmin
{
    public function handle($request, Closure $next)
    {
        $user = $request->user();

        //Можно было сделать, например, через bouncer, ролями и тд, но в данном случае обойдемся полем
        if ($user && $user->is_admin) {
            return $next($request);
        }

        return abort(403);

    }
}
