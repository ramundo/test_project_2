<?php

namespace App\Modules\Beer\Controllers\API;

use App\Modules\Beer\Requests;
use App\Modules\Beer\Resources\BeerResource;
use App\Modules\Beer\Services\CrudService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use App\Models\Beer;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;

class CrudController
{
    /**
     * @var CrudService
     */
    protected $crudService;

    public function __construct(CrudService $crudService)
    {
        $this->crudService = $crudService;
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function index(Requests\IndexRequest $request): AnonymousResourceCollection
    {
        $perPage = $request->input('per_page', 10);
        $beers = $this->crudService->index($perPage);

        return BeerResource::collection($beers);
    }

    /**
     * @param Requests\CreateRequest $request
     * @return JsonResponse
     * @throws \Exception
     */
    public function store(Requests\CreateRequest $request): JsonResponse
    {
        $data = $request->validated();
        $beer = $this->crudService->store($data);

        return response()->json([
            'message' => 'ok',
            'payload' => new BeerResource($beer),
        ]);
    }

    /**
     * @param Requests\UpdateRequest $request
     * @return JsonResponse
     */
    public function update(Requests\UpdateRequest $request): JsonResponse
    {
        $id = $request->route('id');
        $data = $request->validated();
        $beer = $this->crudService->update($id, $data);

        return response()->json([
            'message' => 'ok',
            'payload' => new BeerResource($beer),
        ]);
    }

    /**
     * @param Requests\ShowRequest $request
     * @return JsonResponse
     */
    public function show(Requests\ShowRequest $request): JsonResponse
    {
        $id = $request->route('id');
        $beer = Beer::find($id);

        return response()->json([
            'message' => 'ok',
            'payload' => new BeerResource($beer),
        ]);
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function destroy(Request $request): JsonResponse
    {
        $id = $request->route('id');
        $this->crudService->destroy($id);

        return response()->json([
            'message' => 'ok',
        ]);
    }
}
