<?php

namespace App\Modules\Beer\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateRequest extends FormRequest
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    protected function prepareForValidation()
    {
        $this->merge(['id' => $this->route('id')]);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */

    public function rules()
    {
        $id = $this->route('id');

        return [
            'id' => 'required|integer|exists:beers,id',
            'name' => 'string|min:3|max:255|unique:beers,name,' . $id,
            'description' => 'string|max:255|nullable',
            'image' => 'image|mimes:jpeg|max:2048',
        ];
    }

    public function messages()
    {
        return [

        ];
    }
}

