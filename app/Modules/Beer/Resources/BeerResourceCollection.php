<?php


namespace App\Modules\Beer\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;

class BeerResourceCollection extends ResourceCollection
{
    public function toArray($request)
    {
        return [
            'data' => $this->collection,
            'links' => [
                'self' => 'link-value',
            ],
        ];
    }
}