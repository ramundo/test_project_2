<?php


namespace App\Modules\Beer\Services;

use App\Models\Beer;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Facades\File;


class CrudService
{
    /**
     * @param int $perPage
     * @return LengthAwarePaginator
     */
    public function index(int $perPage): LengthAwarePaginator
    {

        return Beer::paginate($perPage);
    }

    /**
     * @param array $data
     * @return Beer
     * @throws \Exception
     */
    public function store(array $data): Beer
    {
        $beer = new Beer();
        $data = $this->handleData($data);
        $beer->fill($data)->save();

        return $beer;
    }

    /**
     * @param int $id
     * @param array $data
     * @return Beer
     * @throws \Exception
     */
    public function update(int $id, array $data): Beer
    {
        $beer = Beer::find($id);
        $this->deleteBeerImage($beer->image);
        $data = $this->handleData($data);
        $beer->fill($data)->save();

        return $beer;
    }

    /**
     * @param int $id
     */
    public function destroy(int $id): void
    {
        $beer = Beer::find($id);

        if ($beer) {
            $this->deleteBeerImage($beer->image);
            $beer->delete();
        }
    }

    /**
     * @param string|null $imagePath
     */
    protected function deleteBeerImage(string $imagePath = null): void
    {
        if ($imagePath) {
            try {
                unlink(base_path() . $imagePath);
            } catch (\Throwable $e) {
                report($e);
            }
        }
    }

    /**
     * @param $file
     * @return string|null
     * @throws \Exception
     */
    protected function storeImage($file): ?string
    {
        if (!$file) {
            return null;
        }

        $basePath = 'app/public/images';
        $destinationPath = storage_path($basePath);
        $extension = substr($file->getClientOriginalName(), strrpos($file->getClientOriginalName(), '.') + 1);
        $name = 'image_' . bin2hex(random_bytes(10)) . '.' . $extension;

        if (!is_dir($destinationPath)) {
            File::makeDirectory($destinationPath, 0755, true);
        }

        $file->move($destinationPath, $name);
        $path = '/storage/' . $basePath . '/' . $name;

        return $path;
    }

    /**
     * @param array $data
     * @return array
     * @throws \Exception
     */
    protected function handleData(array $data): array
    {
        if (isset($data['image'])) {
            $data['image'] = $this->storeImage($data['image']);
        }

        return $data;
    }
}